<?php

namespace bronikowski\KrsApi;

define('BASE_URL', 'https://api-v3.mojepanstwo.pl/dane/');

class KRS {

    private $data = null;
    private static $dictionaryMapping = array(
        'krs_podmioty.gmina_id' => array(
            'url' => 'https://api-v3.mojepanstwo.pl/dane/gminy/',
            'id' => 'gminy.id',
            'str' => 'gminy.nazwa',
        ),
        'krs_podmiotyo.powiat_id' => array(
            'url' => 'https://api-v3.mojepanstwo.pl/dane/powiaty/',
            'id' => 'powiaty.id',
            'str' => 'powiaty.nazwa',
        ),
        'krs_podmioty.wojewodztwo_id' => array(
            'url' => 'https://api-v3.mojepanstwo.pl/dane/wojewodztwa/',
            'id' => 'wojewodztwa.id',
            'str' => 'wojewodztwa.nazwa',
        ),
        'krs_podmioty.forma_prawna_id' => array(
            'url' => 'https://api-v3.mojepanstwo.pl/dane/krs_formy_prawne/',
            'id' => 'krs_formy_prawne.id',
            'str' => 'krs_formy_prawne.nazwa',
        ),
    );

    public function __construct($data){
        $this->data = $data;
        $this->headers = array('Accept' => 'application/json');
    }

    public static function byKRS(string $krs){
        return self::byLookup('krs', $krs);
    }

    public static function byREGON(string $regon) {
        return self::byLookup('regon', $regon);
    }

    public static function byNIP(string $nip){
        return self::byLookup('nip', $nip);
    }

    private function findDataset(string $name) : \stdClass {

        foreach($this->data->Dataobject as $do){
            if($do->dataset == $name){
                return $do;
            }
        }
        throw new \Exception("Nothing found");
    }

    public function dataset(string $name): array{
        $dataset = (array) $this->findDataset($name);
        $dataset['data'] = (array) $dataset['data'];
        if(count($dataset['data']) > 0){
            foreach(self::$dictionaryMapping as $key => $dm){
                if(array_key_exists($key, $dataset['data'])) {
                    $dataset['data'][ str_replace('_id', '', $key) ] = self::dictionary($key, $dataset['data'][$key]);
                }
            }
        }
        return $dataset;
    }

    public static function dictionaries(){
    }

    public static function dictionary(string $dictionaryName, int $valueId): string {
        $configuration = self::$dictionaryMapping[ $dictionaryName ];
        $response = \Requests::get($configuration['url'].$valueId );
        $dictionary = (array) json_decode($response->body);
        if(!isset($dictionary['data'])){
            dd($dictionary);
        }
        $dictionary['data'] = (array) $dictionary['data'];
        return $dictionary['data'][ $configuration['str'] ];
    }

    public function __get($key) : string {
        $namespaced_key = sprintf('krs_podmioty.%s', $key);
        $dataset = $this->dataset('krs_podmioty');
        return $dataset['data'][$namespaced_key];
    }

    private static function byLookup(string $field, string $id){
        if(intval($id) == 0){
            throw \Exception("String can't be casted to int");
        }
        $partial = sprintf('conditions[krs_podmioty.%s]=%s', $field, $id);
        $query_url = BASE_URL . 'krs_podmioty.json?' . $partial;
        $response = \Requests::get($query_url);
        return new KRS(json_decode($response->body));
    }
}
